﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    [SerializeField] GameObject targetsRoot;
    [SerializeField] GameObject bottlesRoot;
    [SerializeField] Target[] targets;
    [SerializeField] Target[] bottles;
    bool useBottles;

    public void SwitchTargets(bool toggleBottles) {
        useBottles = toggleBottles;
        targetsRoot.SetActive(!useBottles);
        bottlesRoot.SetActive(useBottles);
    }

    public void Reset() {
        Target[] targetList = useBottles ? bottles : targets;
        foreach (Target t in targetList) {
            t.ResetTransform();
        }
    }
}
