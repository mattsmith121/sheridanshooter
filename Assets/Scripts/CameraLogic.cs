﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLogic : MonoBehaviour
{
    [Header("Helper Refs")]
    [SerializeField] GameObject cameraPosition;
    [SerializeField] GameObject cameraLookAt;
    [Tooltip("Lerp speed")]
    [SerializeField] float t;

    [Header("Unity Chan Refs")]
    [Tooltip("Using store/free assets")]
    public bool unityChanActive;
    [SerializeField] GameObject cameraPositionUC;
    [SerializeField] GameObject cameraLookAtUC;

    GameObject lookObj;

    void Start() {
        lookObj = new GameObject();
    }

    void CameraLogicUpdate() {
        // seek position to location
        // seek our look at to camera look at location

        Transform pos = unityChanActive ? cameraPositionUC.transform : cameraPosition.transform;
        Transform look = unityChanActive ? cameraLookAtUC.transform : cameraLookAt.transform;

        Vector3 vNewPos = Vector3.Lerp(transform.position, pos.position, t);
        transform.position = vNewPos;

        Vector3 vNewLookPos = Vector3.Lerp(lookObj.transform.position, look.position, t);
        lookObj.transform.position = vNewLookPos;

        transform.LookAt(lookObj.transform.position);
    }

    void Update() {
        CameraLogicUpdate();
    }

    public void UseUnityChan(bool active) {
        unityChanActive = active;
    }
}
