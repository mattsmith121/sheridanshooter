﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    Vector3 initPos;
    Quaternion initRot;

    void Start() {
        // Store initial position and rotation
        initPos = transform.position;
        initRot = transform.rotation;
    }

    // Reset target for new game
    public void ResetTransform() {
        transform.position = initPos;
        transform.rotation = initRot;
        RemoveForce();
    }

    void RemoveForce() {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        if (rigidbody == null) {
            Debug.LogError("Target doesn't have rigidbody");
            return;
        } 
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }
}
