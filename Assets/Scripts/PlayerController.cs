﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] Transform spawnLocation;
    [Tooltip("The transform used for calculating bullet direction")]
    [SerializeField] Transform directionLocation;
    [SerializeField] Transform gunTransform;

    [Header("Variables for Tweaking")]
    [SerializeField] float bulletSpeed;
    [SerializeField] float playerTurnSpeed;
    [SerializeField] float gunTurnSpeed;

    [Header("Sound Effects")]
    [SerializeField] public AudioClip shootSound;

    // Update is called once per frame
    void Update()
    {
        // Check if game is still active
        if (GameManager.instance.isGameOver()) {
            return;
        }

        // Spacebar pressed, shoot bullet
        if (Input.GetKeyDown(KeyCode.Space)) {
            // Spawn bullet at spawnLocation
            GameObject bullet = Instantiate(bulletPrefab, spawnLocation.position, Quaternion.identity);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            if (rb == null) {
                rb = bullet.AddComponent<Rigidbody>();
            }

            // The direction is vector along the length of the gun
            Vector3 direction = (spawnLocation.position - directionLocation.position).normalized;
            rb.AddForce(direction * bulletSpeed, ForceMode.Impulse);

            // Play sound effect
            AudioSource audio = GetComponent<AudioSource>();
            if (audio != null) {
                audio.PlayOneShot(shootSound);
            }
        }

        // Aim player
        // Down - move gun
        if (Input.GetKey(KeyCode.DownArrow)) {
            gunTransform.Rotate(new Vector3(gunTurnSpeed, 0f, 0f));
        }

        // Up - move gun
        if (Input.GetKey(KeyCode.UpArrow)) {
            gunTransform.Rotate(new Vector3(-gunTurnSpeed, 0f, 0f));
        }

        // Right - turn player
        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Rotate(new Vector3(0f, playerTurnSpeed, 0f));
        }

        // Left - turn player
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.Rotate(new Vector3(0f, -playerTurnSpeed, 0f));
        }

    }
}
