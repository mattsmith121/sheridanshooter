﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanControls : MonoBehaviour
{
    [SerializeField] Transform leftArm;
    [SerializeField] Transform rightArm;
    [SerializeField] float turnSpeed = 0.1f;

    // Unity Chan also moves her arms
    // *Note*: the arm setup is kind of jank do to me posing Unity Chan
    // so the rotations are opposite for the right arm
    void Update()
    {
        // Check if game is still active
        if (GameManager.instance.isGameOver()) {
            return;
        }

        // Down - move arms
        if (Input.GetKey(KeyCode.DownArrow)) {
            leftArm.Rotate(new Vector3(turnSpeed, 0f, 0f));
            rightArm.Rotate(new Vector3(-turnSpeed, 0f, 0f));
        }

        // Up - move arms
        if (Input.GetKey(KeyCode.UpArrow)) {
            leftArm.Rotate(new Vector3(-turnSpeed, 0f, 0f));
            rightArm.Rotate(new Vector3(turnSpeed, 0f, 0f));
        }
    }
}
