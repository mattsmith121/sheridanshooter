﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    [Header("UI Components")]
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] GameObject endGameMessage;
    [SerializeField] GameObject startGameScreen;

    [Header("Player Objects")]
    [SerializeField] GameObject unityChan;
    [SerializeField] GameObject capsulePlayer;

    [Header("Targets")]
    [SerializeField] TargetManager targetManager;

    [Header("Main Camera")]
    [SerializeField] CameraLogic mainCamera;

    public static GameManager instance;
    static int score;

    // Start is called before the first frame update
    void Start() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(this);
            return;
        }

        ResetGame();
    }

    void ResetGame() {
        // Initialize score
        score = 0;
        UpdateScoreText();

        // Reset targets
        targetManager.Reset();

        // Hide End Game Screen
        endGameMessage.SetActive(false);
    }

    public void IncreaseScore() {
        score++;
        UpdateScoreText();

        if (score >= 20) {
            endGameMessage.SetActive(true);
        }
    }

    public int GetScore() {
        return score;
    }

    public bool isGameOver() {
        return score >= 20;
    }

    public void Play() {
        ResetGame();
        startGameScreen.SetActive(false);
    }

    public void PlayAgain() {
        ResetGame();
    }

    public void Exit() {
        Application.Quit();
    }

    public void MainMenuClick() {
        endGameMessage.SetActive(false);
        startGameScreen.SetActive(true);
    }

    public void UseUnityAssets(bool toggle) {
        targetManager.SwitchTargets(toggle);
        mainCamera.UseUnityChan(toggle);
        unityChan.SetActive(toggle);
        capsulePlayer.SetActive(!toggle);
    }

    #region Helpers

    void UpdateScoreText() {
        scoreText.text = "Score: " + score;
    }

    #endregion


}
