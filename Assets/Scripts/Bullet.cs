﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [Header("Sound Effects")]
    [SerializeField] AudioClip[] hitSounds;
    [SerializeField] AudioClip missSound; 
    bool collided;

    void Start() {
        // Destroy after 4 seconds
        Invoke("Destroy", 4f);
    }

    void OnCollisionEnter(Collision collision) {
        // Only care about first collision
        if (collided)
            return;

        collided = true;

        bool targetHit = false;
        // Did we hit a target?
        if (collision.collider.GetComponent<Target>() != null) {
            targetHit = true;
            // Increase score
            GameManager.instance.IncreaseScore();
        }

        AudioSource audio = GetComponent<AudioSource>();
        if (audio != null) {
            if (!targetHit) {
                audio.PlayOneShot(missSound);
            }
            else {
                int rand = Random.Range(0, hitSounds.Length);
                audio.PlayOneShot(hitSounds[rand]);
            }
        }
    }

    void FixedUpdate() {
        // Want to remove balls when game ends
        // this way they don't interfere if new game started quickly
        if (GameManager.instance.GetScore() >= 20) {
            Destroy(gameObject);
        }
    }

    void Destroy() {
        Destroy(gameObject);
    }
}
